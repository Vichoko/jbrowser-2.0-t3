package test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;
import sh4j.model.command.SSortClassesByHierarchy;
import sh4j.model.command.SSortClassesByName;
import sh4j.model.command.SSortPackagesByName;
//import sh4j.model.command.SSortClassesByHierarchy;
//import sh4j.model.command.SSortClassesByName;
//import sh4j.model.command.SSortPackagesByName;
import sh4j.parser.SClassParser;

public class SSortTest {

  @Test
  public void packagesByName() {
    SProject project = new SProject();
    project.addPackage(new SPackage("b"));
    project.addPackage(new SPackage("a"));
    project.addPackage(new SPackage("d"));
    project.addPackage(new SPackage("c"));
    SSortPackagesByName sort = new SSortPackagesByName();
    String[] expected = new String[] { "a", "b", "c", "d" };
    assertEquals(sort.toString(), "Sort Packages By Name");
    sort.executeOn(project);
    String[] result = toStringArray(project.packages().toArray());
    assertArrayEquals(expected, result);
  }

  @Test
  public void classesByName() {
    SProject project = new SProject();
    SPackage pkg = new SPackage("pack");
    project.addPackage(pkg);
    List<SClass> cls = SClassParser.parse(" class A{} class B{} class C{}");
    for (SClass c : cls) {
      pkg.addClass(c);
    }
    SSortClassesByName sort = new SSortClassesByName();
        
    sort.executeOn(project);
    assertEquals(sort.toString(), "Sort Classes By Name");
    String[] result = toStringArray(pkg.classes().toArray());
    String[] expected = new String[] { "A", "B", "C" };
    assertArrayEquals(expected, result);
  }

  @Test
  public void classesByAbstractHierarchy() {
    SProject project = new SProject();
    SPackage pkg = new SPackage("pack");
    SPackage pkg2 = new SPackage("pack2");
    project.addPackage(pkg);
    project.addPackage(pkg2);
    List<SClass> cls = SClassParser.parse(" class Z{} class B extends Z{} class A{}");
    List<SClass> cls2 = SClassParser
        .parse("class F extends E{} class G extends F{} class E{} class C extends B{} class B{} class A{}");
    for (SClass c : cls) {
      pkg.addClass(c);
    }
    for (SClass c : cls2) {
      pkg2.addClass(c);
    }
    SSortClassesByHierarchy sort = new SSortClassesByHierarchy();
    sort.executeOn(project);
    assertEquals(sort.toString(), "Sort Classes By Hierarchy");
    String[] result = toStringArray(pkg.classes().toArray());
    String[] result2 = toStringArray(pkg2.classes().toArray());
    ;
    print(result);
    print(result2);
    String[] expected = new String[] { "A", "Z", "  B" };
    String[] expected2 = new String[] { "A", "B", "  C", "E", "  F", "    G" };
    assertArrayEquals(expected, result);
    assertArrayEquals(expected2, result2);
  }

  @Test
  public void classesByImplementHierarchy() {
    SProject project = new SProject();
    SPackage pkg = new SPackage("pack");
    SPackage pkg2 = new SPackage("pack2");
    project.addPackage(pkg);
    project.addPackage(pkg2);
    List<SClass> cls = SClassParser.parse(" class Z{} class B implements Z{} class A{}");
    List<SClass> cls2 = SClassParser
        .parse("class F implements E{} class G implements F{} class E{} class C implements B{} class B{} class A{}");
    for (SClass c : cls) {
      pkg.addClass(c);
    }
    for (SClass c : cls2) {
      pkg2.addClass(c);
    }
    (new SSortClassesByHierarchy()).executeOn(project);
    String[] result = toStringArray(pkg.classes().toArray());
    String[] result2 = toStringArray(pkg2.classes().toArray());
    ;
    print(result);
    print(result2);
    String[] expected = new String[] { "A", "B", "Z" };
    String[] expected2 = new String[] { "A", "B", "C", "E", "F", "G" };
    assertArrayEquals(expected, result);
    assertArrayEquals(expected2, result2);
  }

  @Test
  public void classesByHierarchy_2() {
    SProject project = new SProject();
    SPackage pkg = new SPackage("browser");
    project.addPackage(pkg);
    List<SClass> cls = SClassParser.parse(
        " class SClass implements SObject{} class SFactory{} class SMethod implements SObject{} class SObject{} class SPackage implements SObject {} class SProject implements SObject{}");
    for (SClass c : cls) {
      pkg.addClass(c);
    }
    (new SSortClassesByHierarchy()).executeOn(project);
    String[] result = toStringArray(pkg.classes().toArray());

    print(result);
    String[] expected = new String[] { "SClass", "SFactory", "SMethod", "SObject", "SPackage", "SProject" };
    assertArrayEquals(expected, result);
  }

  @Test
  public void classesByHierarchyMixed() {
    SProject project = new SProject();
    SPackage pkg = new SPackage("browser");
    project.addPackage(pkg);
    List<SClass> cls = SClassParser.parse(
        " class SZlass implements SObject{} class SFactory{} class SMethod extends SZlass{} class SObject{} class SPackage extends SZlass {} class SProject extends SZlass{}");
    for (SClass c : cls) {
      pkg.addClass(c);
    }
    (new SSortClassesByHierarchy()).executeOn(project);
    String[] result = toStringArray(pkg.classes().toArray());

    print(result);
    String[] expected = new String[] { "SFactory", "SObject", "SZlass", "  SMethod", "  SPackage", "  SProject" };
    assertArrayEquals(expected, result);
  }

  public String[] toStringArray(Object[] array) {
    String[] result = new String[array.length];
    for (int i = 0; i < result.length; i++) {
      result[i] = array[i].toString();
    }
    return result;
  }

  public void print(Object[] objs) {
    for (Object o : objs) {
      System.out.print("\"" + o.toString() + "\",");
    }
    System.out.println();
  }
}
