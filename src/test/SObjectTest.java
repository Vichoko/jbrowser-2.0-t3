package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SMethod;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;
import sh4j.parser.SClassParser;

public class SObjectTest {
  SProject project;
  SPackage pkg;

  @Before
  public void setUp() throws Exception {
    project = new SProject();
    pkg = new SPackage("pkg");
    project.addPackage(pkg);
    List<SClass> cls = SClassParser.parse("class A{}");
    pkg.addClass(cls.get(0));
  }

  @Test
  public void projectTest() {
    assertTrue(project.icon() == null);

    SPackage ret = project.get("pkg");
    assertEquals(pkg, ret);
    assertEquals(ret.toString(), "pkg");

    List<SPackage> packages = project.packages();
    assertTrue(!packages.isEmpty());
    assertTrue(packages.size() == 1);
    SPackage pkg2 = new SPackage("pkg2");
    project.addPackage(pkg2);
    assertTrue(packages.size() == 2);
  }

  @Test
  public void packageTest() {
    assertTrue(!pkg.classes().isEmpty());
    assertTrue(pkg.classes().size() > 0);
    assertEquals(pkg.toString(), "pkg");
    assertTrue(pkg.compareTo(pkg) == 0);
  }

  @Test
  public void classTest() {
    List<SClass> classes = pkg.classes();
    SClass testClass = classes.get(0);
    assertTrue(!testClass.isInterface());
    assertEquals(testClass.className(), "A");
    assertEquals(testClass.superClass(), "Object");
    assertEquals(testClass.className(), testClass.toString());
    assertTrue(testClass.compareTo(testClass) == 0);
  }

  @Test
  public void methodTest() {
    List<SClass> cls = SClassParser.parse("class A{ private void sut(int a){return a;}}");
    pkg.addClass(cls.get(0));
    assertTrue(pkg.classes().size() == 2);
    SClass testClass = pkg.classes().get(1);
    List<SMethod> methods = testClass.methods();
    assertTrue(methods.size() == 1);
    SMethod method = methods.get(0);
    assertEquals(method.modifier(), "private");
    assertEquals(method.name(), "sut");
    assertEquals(method.name(), method.toString());
    assertTrue(method.getLinesOfCode() != 0);
  }

}
