package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;
import sh4j.model.command.SBredCommand;
import sh4j.model.command.SCommand;
import sh4j.model.command.SDarkCommand;
import sh4j.model.command.SEclipseCommand;
import sh4j.model.command.SNOfClassesFooter;
import sh4j.model.command.SNOfLinesFooter;
import sh4j.model.command.SNOfWarningsFooter;
import sh4j.model.highlight.SHighlighter;
import sh4j.model.style.SStyle;
import sh4j.parser.SClassParser;

public class SCommandTest {

  SProject project;
  
  @Before
  public void setUp() throws Exception {
    project = new SProject();
    SPackage pkg = new SPackage("pack");
    List<SClass> cls = SClassParser
        .parse("class A{ public int foo(){\n int a;\n String b;\n int c=this.a+b; return c; }}");
    SClass clase = cls.get(0);
    
    project.addPackage(pkg);
    pkg.addClass(clase);
    
  }
  
  @Test
  public void testSNOfClasses(){
    SCommand command = new SNOfClassesFooter();
    command.executeOn(project);
    String header = "Numero de clases: ";
    assertEquals(command.toString(), header + 1);
  }
  
  @Test
  public void testSNOfLines(){
    SCommand command = new SNOfLinesFooter();
    command.executeOn(project);
    String header = "Numero total de lineas: ";
    assertEquals(command.toString(), header + 7);
  }
  
  @Test
  public void testSNOfWarnings(){
    SCommand command = new SNOfWarningsFooter();
    command.executeOn(project);
    String header = "Numero de warnings: ";
    assertEquals(command.toString(), header + 0);
  }
  
  @Test
  public void testDarkStyleCommand(){
    SDarkCommand command = new SDarkCommand();
    SStyle style = command.getStyle();
    SHighlighter[] hgs = command.getHighlights();
    assertEquals(style.toString(), "dark");
    assertEquals(command.toString(), command.name());
    assertEquals(command.toString(), "Dark Style");
    assertTrue(hgs.length != 0);
  }
  
  @Test
  public void testEclipseStyleCommand(){
    SEclipseCommand command = new SEclipseCommand();
    SStyle style = command.getStyle();
    SHighlighter[] hgs = command.getHighlights();
    assertEquals(style.toString(), "eclipse");
    assertEquals(command.toString(), command.name());
    assertEquals(command.toString(), "Eclipse Style");
    assertTrue(hgs.length != 0);
  }
  
  @Test
  public void testBredStyleCommand(){
    SBredCommand command = new SBredCommand();
    SStyle style = command.getStyle();
    SHighlighter[] hgs = command.getHighlights();
    assertEquals(style.toString(), "bred");
    assertEquals(command.toString(), command.name());
    assertEquals(command.toString(), "Bred Style");
    assertTrue(hgs.length != 0);
  }

}
