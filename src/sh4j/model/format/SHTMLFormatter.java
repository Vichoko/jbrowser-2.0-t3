package sh4j.model.format;

import sh4j.model.highlight.SCR;
import sh4j.model.highlight.SDummy;
import sh4j.model.highlight.SHighlighter;
import sh4j.model.style.SEclipseStyle;
import sh4j.model.style.SStyle;
import sh4j.parser.model.SBlock;
import sh4j.parser.model.SText;

/**
 * It formats the parsed input String (SBlock) and returns a String with the
 * respecting HTML code of the respecting Style. It has a series of methods to
 * construct the HTML code in buffer.
 */
public class SHTMLFormatter implements SFormatter {
  private final StringBuffer buffer;
  private int level;
  private int lineno;
  private final SStyle style;
  private final SHighlighter[] highlighters;
  private boolean linenumbers;

  /**
   * Constructor of the class. It recieves a Style and an array of Highlighters.
   * It also recieves a linenumbers state var. When this var is true, then this
   * formatter includes the line numbers in the final formatted code. And if it
   * is false, then it doesn't display the line numbers.
   */
  public SHTMLFormatter(SStyle style, boolean linenumbers, SHighlighter... hs) {
    this.lineno = 0;
    this.style = style;
    highlighters = hs;
    buffer = new StringBuffer();
    this.linenumbers = linenumbers;
  }

  /**
   * This constructor is used with a default style (SEclpiseStyle) and without
   * highlights. Its purpose is to adquire the number of lines the class have;
   * you can see them with getLines()
   */
  public SHTMLFormatter() {
    this.lineno = 0;
    this.style = new SEclipseStyle();
    highlighters = new SHighlighter[0];
    buffer = new StringBuffer();
  }

  private SHighlighter lookup(String text) {
    for (SHighlighter h : highlighters) {
      if (h.needsHighLight(text)) {
        return h;
      }
    }
    return new SDummy();
  }

  @Override
  public void styledWord(String word) {
    buffer.append(lookup(word).highlight(word, style));
  }

  @Override
  public void styledChar(char character) {
    buffer.append(lookup(character + "").highlight(character + "", style));
  }

  @Override
  public void styledSpace() {
    buffer.append(' ');
  }

  @Override
  public void styledCR() {
    lineno++;
    buffer.append("\n");
    /*
     ** The next append adds the linenumber ('lineno') to the HTMLFormat. it uses
     * SCR (Carriage Return's Highligh) higlight method to format the line
     * numbers depending of the style. It displays the line number depending of
     * the state of the linenumbers variable.
     */
    if (linenumbers) {
      buffer.append(SCR.highlight("  " + Integer.toString(lineno) + " ", style));
    }
    indent();

  }

  /** It returns the quantity of lines of the exported block. */
  public int getLines() {
    return lineno;
  }

  @Override
  public void styledBlock(SBlock block) {
    level++;
    for (SText text : block.texts()) {
      text.export(this);
    }
    level--;
  }

  @Override
  public void indent() {
    for (int i = 0; i < level; i++) {
      buffer.append("  ");
    }
  }

  @Override
  public String formattedText() {
    return style.formatBody(buffer.toString());
  }

  /**
   * It returns a formatted string containing the html code acording to the tag
   * name, style and content.
   */
  public static String tag(String name, String content, String style) {
    return "<" + name + " style='" + style + "'>" + content + "</" + name + ">";
  }
}
