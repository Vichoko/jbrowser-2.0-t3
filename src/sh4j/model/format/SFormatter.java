package sh4j.model.format;

import sh4j.parser.model.SBlock;

/**
 * Interface that format a SBlock (Parsed input String) to a formatted String
 * (With the correct highlights); it has 2 format types HTML
 * (SHTMLFormatter.java) & Plain (SPlainFormatter.java)
 */
public interface SFormatter {
  /**
   * It takes a String word and appends it to the StringBuffer highlighting it
   * with the correct Highlight depending of the SStyle. in SPlainFormater it
   * appends it in plain text (without highlight).
   */
  public void styledWord(String word);

  /**
   * It takes a Char character and appends it to the StringBuffer highlighting
   * it with the correct SHighlight depending of the SStyle. in SplainFormater
   * it appends it in plain text (without highlight).
   */
  public void styledChar(char character);

  /** It appends a space(' ') to the StringBuffer. */
  public void styledSpace();

  /**
   * It appends a Carried Return (\n) to the StringBuffer. It also count the
   * lines of code.
   */
  public void styledCR();

  /** It indents the code depending of the level. */
  public void indent();

  /**
   * It takes a SBlock block, then it iterates over the block texts exporting
   * each one to the formatter. Taking count of the level to keep the correct
   * indentation.
   */
  public void styledBlock(SBlock block);

  /**
   * Returns a String of the formated text (HTML or Plain) appended in the
   * StringBuffer, depending of the Style (SPlainFormater returns the
   * StringBuffer in a String without style).
   */
  public String formattedText();
}
