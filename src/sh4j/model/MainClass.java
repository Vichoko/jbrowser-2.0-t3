package sh4j.model;

import sh4j.model.command.SBredCommand;
import sh4j.model.command.SDarkCommand;
import sh4j.model.command.SEclipseCommand;
import sh4j.model.command.SNOfClassesFooter;
import sh4j.model.command.SNOfLinesFooter;
import sh4j.model.command.SNOfWarningsFooter;
import sh4j.model.command.SSortClassesByHierarchy;
import sh4j.model.command.SSortClassesByName;
import sh4j.model.command.SSortPackagesByName;
import sh4j.model.highlight.SClassName;
import sh4j.model.highlight.SCurlyBracket;
import sh4j.model.highlight.SKeyWord;
import sh4j.model.highlight.SMainClass;
import sh4j.model.highlight.SModifier;
import sh4j.model.highlight.SPseudoVariable;
import sh4j.model.highlight.SSemiColon;
import sh4j.model.highlight.SString;
import sh4j.model.style.SEclipseStyle;
import sh4j.ui.SFrame;

import java.io.IOException;

/**
 * Initializes the visual interface permitting modify parameters like Style and
 * Highlights. Also enable to add new Commands.
 */
public abstract class MainClass {
  /**
   * The codesmell detector requires that an abstract class have a abstract
   * method. For the other hand, to only have static methods it is needed to
   * have its in an abstract class.
   */
  protected abstract void none();

  /**
   * The main of the program. It initializes the GUI with stablished parameters.
   */
  public static void main(String[] args) throws IOException {
    SFrame frame = new SFrame(new SEclipseStyle(), new SClassName(), new SCurlyBracket(), 
        new SKeyWord(), new SMainClass(), new SModifier(), new SPseudoVariable(), 
        new SSemiColon(), new SString());
    frame.pack();
    frame.setVisible(true);
    frame.addCommands(new SSortClassesByHierarchy(), new SSortClassesByName(), 
        new SSortPackagesByName());
    frame.addStyles(new SEclipseCommand(), new SDarkCommand(), new SBredCommand());
    frame.addFooters(new SNOfLinesFooter(), new SNOfClassesFooter(), new SNOfWarningsFooter());

  }

}
