package sh4j.model.command;

import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

import java.util.Collections;
import java.util.List;

/** It sorts by Name the List of cPackages contained in the Project. **/
public class SSortPackagesByName extends SCommand {

  @SuppressWarnings("unchecked")
  @Override
  public void executeOn(SProject project) {
    List<SPackage> packages = project.packages();
    /** SPackage overrided the method compareTo to compare by package names. **/
    Collections.sort(packages);

  }

  @Override
  public String toString() {
    return "Sort Packages By Name";
  }
}
