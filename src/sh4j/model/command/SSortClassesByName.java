package sh4j.model.command;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

import java.util.Collections;
import java.util.List;

/**
 * It sorts by Name the List of classes of each Package contained in the
 * Project.
 **/
public class SSortClassesByName extends SCommand {

  @SuppressWarnings("unchecked")
  @Override
  public void executeOn(SProject project) {
    List<SPackage> packages = project.packages();
    for (int i = 0; i < packages.size(); i++) {
      List<SClass> classes = packages.get(i).classes();
      /** SClass overrided the method compareTo to compare by package names. **/
      Collections.sort(classes);

      /** Restore the indentation for this kind of sort. */
      for (SClass c : classes) {
        c.setIndentation(0);
      }
    }

  }

  @Override
  public String toString() {
    return "Sort Classes By Name";
  }

}
