package sh4j.model.command;

import sh4j.model.highlight.SHighlighter;
import sh4j.model.style.SStyle;

/**
 * It represent a style that could be applied to a project.
 *
 */
public abstract class SCommandStyle {
  /** It return an array of Highlighters of the corresponding SCommandStyle. */
  public abstract SHighlighter[] getHighlights();

  /** It return the style of the command. */
  public abstract SStyle getStyle();

  /**
   * It returns the title of the command for the visual interface command menu.
   */
  public abstract String toString();

  /** The same as the toString. */
  public String name() {
    return this.toString();
  }
}
