package sh4j.model.command;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class sorts Classes of a Project by Hierarchy. It uses a Tree Structure
 * to create the hierarchical model, then the tree is saved in a List by
 * 'pre-orden'.
 **/
public class SSortClassesByHierarchy extends SCommand {

  @Override
  public void executeOn(SProject project) {
    List<SPackage> packages = project.packages();
    for (int i = 0; i < packages.size(); i++) {
      List<SClass> classes = packages.get(i).classes();
      hierarchySort(classes);
    }
  }

  /**
   * It recieves a List of classes and it creates the Tree Structure containing
   * the classes in a hierarchical model. When the structure is created, it
   * calls sortBySons method to export the Tree to a sorted List. Then the
   * classes list is replaced by this new sorted list.
   */
  private void hierarchySort(List<SClass> classes) {
    /* Begin building the hierarchical structure. */
    HNode object = new HNode(
        "Object"); /*
                    * Object is the head of the hierarchical structure.
                    */
    List<HNode> nodes = new ArrayList<HNode>();
    nodes.add(object);
    /* Fills the nodes array of the nodes with their superclass. */
    for (int i = 0; i < classes.size(); i++) {
      SClass clase = classes.get(i);
      String father;
      father = clase.superClass();
      nodes.add(new HNode(clase, father));
    }

    /**
     * Once every class of the package is in the node array, starts conecting
     * every node with their superclass. The search starts from 1 because of
     * Object doesn't have any superclass.
     */
    for (int i = 1; i < nodes.size(); i++) {
      HNode actualNode = nodes.get(i);
      /* Now looks for it's superclass */
      for (int j = 0; j < nodes.size(); j++) {
        HNode check = nodes.get(j);
        /*
         ** If it finds it's superclass. The algorithm completes the info of the
         * HNode. The son (actual_node) inherits from it's superclass. The
         * father (check) has a new son: (actual_node).
         */
        if (actualNode.father().equals(check.toString())) {
          actualNode.inheritsFrom(check);
          /* This model only allows one superclass or interface. */
          break;
        }
      }
    }

    /*
     * Now starting from Object (The first element of the nodes array). The
     * algorithm put the classes in hierarchical order
     */
    HNode head = nodes.get(0);
    List<SClass> newList = sortBySons(head, 0);
    classes.clear();
    classes.addAll(newList);
  }

  /**
   * This method recieves a well-done hierarchical tree of HNodes and exports it
   * to a newList crossing the Tree in pre-order.
   */
  private List<SClass> sortBySons(HNode superClass, int indentation) {
    List<SClass> newList = new ArrayList<SClass>();
    List<HNode> subClasses = superClass.subclasses;
    /*
     * For each son, it's own hierarchy is saved. The son with the minor
     * lexicographic value of his name it's saved first. After being saved, the
     * son is deleted of the father's subclass list.
     */
    Collections.sort(subClasses);
    while (!subClasses.isEmpty()) {
      /*
       * Now the array is sorted alphabeticly by classnames.
       */
      HNode minSubClass = subClasses.get(0);
      /*
       * The class is added to the list, then it's sons; finally removing the
       * class of the list.
       */
      SClass actualClass = minSubClass.actualClass;
      /** The actualClass is set with it correct indentation level. */
      actualClass.setIndentation(indentation);

      /**
       * Since the indentation level is usually low, there is no concern in
       * making a buffer to append the spaces.
       */

      newList.add(minSubClass.actualClass);
      newList.addAll(sortBySons(minSubClass, indentation + 1));
      subClasses.remove(minSubClass);
    }
    return newList;

  }

  @Override
  public String toString() {
    return "Sort Classes By Hierarchy";
  }

}
