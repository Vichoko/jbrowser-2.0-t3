package sh4j.model.command;

import sh4j.model.highlight.SClassName;
import sh4j.model.highlight.SCurlyBracket;
import sh4j.model.highlight.SHighlighter;
import sh4j.model.highlight.SKeyWord;
import sh4j.model.highlight.SMainClass;
import sh4j.model.highlight.SModifier;
import sh4j.model.highlight.SPseudoVariable;
import sh4j.model.highlight.SSemiColon;
import sh4j.model.highlight.SString;
import sh4j.model.style.SDarkStyle;
import sh4j.model.style.SStyle;

/**
 * This class provides the highlights and style of the Dark Style. The
 * highlights can be easily modified to create different style effects.
 */
public class SDarkCommand extends SCommandStyle {

  @Override
  public SHighlighter[] getHighlights() {
    SHighlighter[] highlights = { new SClassName(), new SCurlyBracket(), new SKeyWord(), 
        new SMainClass(),new SModifier(), new SPseudoVariable(), new SSemiColon(), 
        new SString() };
    return highlights;
  }

  @Override
  public SStyle getStyle() {
    return new SDarkStyle();
  }

  @Override
  public String toString() {
    return "Dark Style";
  }

}
