package sh4j.model.command;

import sh4j.model.browser.SProject;

/**
 * It represent a command that could be applied to a project.
 * 
 * @author juampi
 *
 */
public abstract class SCommand {

  /** It contains the actions that the Command do on the project. */
  public abstract void executeOn(SProject project);

  /**
   * It returns the title of the command for the visual interface command menu.
   */
  public abstract String toString();

  /** The same as the toString. */
  public String name() {
    return this.toString();
  }
}
