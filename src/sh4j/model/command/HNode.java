package sh4j.model.command;

import sh4j.model.browser.SClass;

import java.util.ArrayList;
import java.util.List;

/**
 * A structure built by HNodes is a hierarchical model of classes. It is used to
 * build a hierarchical tree, with Object class at the head.
 **/
public class HNode implements Comparable<HNode> {
  SClass actualClass;
  protected String actualClassName;

  public HNode father;
  public String fatherName;

  public List<HNode> subclasses;

  int dependants;

  /**
   * Constructor of a node that have a father. Every class have a father, except
   * Object. (Every class inherits from Object)
   */
  public HNode(SClass actualClass, String fatherName) {
    this.actualClass = actualClass;
    this.actualClassName = actualClass.toString();

    this.fatherName = fatherName;
    this.father = null;

    subclasses = new ArrayList<HNode>();

    this.dependants = 0;
  }

  /**
   * This constructor is made specially for initialing the head of the
   * structure: Object. It doesnt have a specific class, neither a father, but
   * it can have subclasses.
   */
  public HNode(String actualClass) {

    this.actualClassName = actualClass;
    this.actualClass = null;
    this.father = null;

    subclasses = new ArrayList<HNode>();

    this.dependants = 0;
  }

  /** It returns the father of the HNode instance. */
  public String father() {
    assert father.toString().equals(fatherName);
    return fatherName;
  }

  /**
   * Set a father class to the current HNode instance. It recieves another HNode
   * object as argument.
   */
  public void inheritsFrom(HNode superclass) {
    this.father = superclass;
    superclass.addSubclass(this);
  }

  /**
   * This method adds a SubClass to the SubClass list of the HNode. When a
   * subclass is added, the dependants number rises.
   */

  public void addSubclass(HNode subclass) {
    this.subclasses.add(subclass);
    this.dependants++;
  }

  /** toString method returns the name of the class that the HNode refers. */
  public String toString() {
    return actualClassName;
  }

  /** It return the number of SubClasses. */
  public int childrenNumber() {
    return dependants;
  }

  /**
   * Provides a way to compare 2 HNode objects by the className each HNode
   * refers. It returns code error -2 if one object isn't an isntsnce of HNode.
   */
  @Override
  public int compareTo(HNode obj) {
    if (obj instanceof HNode) {
      return this.toString().compareTo(obj.toString());
    }
    return -2;
  }

}
