package sh4j.model.command;

import sh4j.model.browser.SProject;
import sh4j.model.visitor.NOfClassesVisitor;

/**
 * This command count the number of classes in a project. It is used to the
 * footer status bar.
 */
public class SNOfClassesFooter extends SCommand {
  private int count = -1;

  @Override
  public void executeOn(SProject project) {
    NOfClassesVisitor visitor = new NOfClassesVisitor();
    project.accept(visitor);
    count = visitor.getCount();
  }

  @Override
  public String toString() {
    return "Numero de clases: " + count;
  }

}
