package sh4j.model.command;

import sh4j.model.browser.SProject;
import sh4j.model.visitor.NOfWarningsVisitor;

/**
 * This command responsability is to count the number of warnings in a project.
 * A warning ocurrs when a method have more than 30 lines of code. It is used to
 * the footer status bar.
 */
public class SNOfWarningsFooter extends SCommand {

  private int count = -1;

  @Override
  public void executeOn(SProject project) {
    NOfWarningsVisitor visitor = new NOfWarningsVisitor();
    project.accept(visitor);
    count = visitor.getCount();
  }

  @Override
  public String toString() {
    return "Numero de warnings: " + count;
  }

}
