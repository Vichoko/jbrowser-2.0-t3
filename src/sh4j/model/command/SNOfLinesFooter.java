package sh4j.model.command;

import sh4j.model.browser.SProject;
import sh4j.model.visitor.NOfLinesVisitor;

/**
 * This command responsability is to count the number of lines in a project. It
 * is used to the footer status bar.
 */
public class SNOfLinesFooter extends SCommand {
  private int count = -1;

  @Override
  public void executeOn(SProject project) {
    NOfLinesVisitor visitor = new NOfLinesVisitor();
    project.accept(visitor);
    count = visitor.getCount();
  }

  @Override
  public String toString() {
    return "Numero total de lineas: " + count;

  }

}
