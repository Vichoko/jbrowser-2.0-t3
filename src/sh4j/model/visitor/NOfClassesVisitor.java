package sh4j.model.visitor;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SMethod;

/** This visitor (operation) counts the number of classes. */
public class NOfClassesVisitor extends AbstractVisitor {
  private int counter;

  /** When it is instantiated, the count starts at 0. */
  public NOfClassesVisitor() {
    counter = 0;
  }

  @Override
  public void visitClass(SClass clase) {
    counter++;
  }

  @Override
  public void visitMethod(SMethod method) {
    /** It does nothing. */
  }

  /** It returns the count of classes. */
  public int getCount() {
    return counter;
  }
}
