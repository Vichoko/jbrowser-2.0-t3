package sh4j.model.visitor;

import sh4j.model.browser.SMethod;

/** This visitor (operation) counts the total number of lines. */
public class NOfLinesVisitor extends AbstractVisitor {
  private int count;

  /** When it's instantiated it counter begins in 0. */
  public NOfLinesVisitor() {
    count = 0;
  }

  @Override
  public void visitMethod(SMethod method) {
    count += method.getLinesOfCode();
  }

  /** It returns the total count of lines. */
  public int getCount() {
    return count;
  }
}
