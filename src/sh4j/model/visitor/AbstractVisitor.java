package sh4j.model.visitor;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SMethod;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

/**
 * AbstractVisitor provides default visitElement methods for standard
 * operations.
 */
public abstract class AbstractVisitor implements Visitor {

  @Override
  public void visitProject(SProject project) {
    for (SPackage pkg : project.packages()) {
      pkg.accept(this);
    }

  }

  @Override
  public void visitPackage(SPackage pack) {
    for (SClass c : pack.classes()) {
      c.accept(this);
    }

  }

  @Override
  public void visitClass(SClass clase) {
    for (SMethod m : clase.methods()) {
      m.accept(this);
    }

  }

  @Override
  public abstract void visitMethod(SMethod method);

}
