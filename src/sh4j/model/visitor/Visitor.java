package sh4j.model.visitor;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SMethod;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

/**
 * Every visitor should have this methods to correctly fill in a visitor
 * responsability. It should be able to visit a project, a package, a class and
 * a method.
 */
public interface Visitor {
  /**
   * It recieves a project and it do something over it, depending of the Visitor
   * operation.
   */
  void visitProject(SProject project);

  /**
   * It recieves a package and it do something over it, depending of the Visitor
   * operation.
   */
  void visitPackage(SPackage pack);

  /**
   * It recieves a class and it do something over it, depending of the Visitor
   * operation.
   */
  void visitClass(SClass clase);

  /**
   * It recieves a method and it do something over it, depending of the Visitor
   * operation.
   */
  void visitMethod(SMethod method);
}
