package sh4j.model.visitor;

import sh4j.model.browser.SMethod;

/**
 * It counts the number of warnings. A warning ocurrs when a method have more
 * than 30 lines of code.
 */
public class NOfWarningsVisitor extends AbstractVisitor {
  private int count;

  /** When this visitor is instantiated the count begins at 0. */
  public NOfWarningsVisitor() {
    count = 0;
  }

  @Override
  public void visitMethod(SMethod method) {
    if (method.getLinesOfCode() > 30) {
      count++;
    }
  }

  /** It returns the count of warnings. */
  public int getCount() {
    return count;
  }
}
