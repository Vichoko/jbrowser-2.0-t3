package sh4j.model.browser;

import sh4j.model.visitor.Visitor;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;


/**
 * SProject wraps the information of a Java Project. Providing methods to obtain
 * information about the project like packages(), get(Package); addPackage to
 * add a new Package to the Project. And the methods SObject interface requires.
 * 
 **/
public class SProject implements SObject {
  private final List<SPackage> packages;

  /** It initiates an SProject initially empty (With no packages). */
  public SProject() {
    packages = new ArrayList<SPackage>();
  }

  /** Method to add packages to the package list of the SProject isntance. */
  public void addPackage(SPackage pack) {
    packages.add(pack);
  }

  /** It returns the package list of the SProject. */
  public List<SPackage> packages() {
    return packages;
  }

  /**
   * This method looks for a Package with the name in the parameter, and if it's
   * found, it is returned. If it's not, return null.
   */
  public SPackage get(String pkgName) {
    for (SPackage pkg : packages) {
      if (pkg.toString().equals(pkgName)) {
        return pkg;
      }
    }
    return null;
  }

  /**
   * A project doesn't have icon. So this method returns null.
   **/
  @Override
  public String icon() {
    return null;
  }

  @Override
  public Font font() {
    return new Font("Helvetica", Font.PLAIN, 12);
  }

  @Override
  public Color background() {
    return Color.WHITE;
  }

  @Override
  public void accept(Visitor visitor) {
    visitor.visitProject(this);
  }

}
