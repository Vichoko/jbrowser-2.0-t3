package sh4j.model.browser;

import sh4j.model.visitor.Visitor;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;


/**
 * SPackage wraps the information of a Java Package. Providing methods to obtain
 * information about the package and a list of classes the package includes.
 **/
@SuppressWarnings("rawtypes")
public class SPackage implements Comparable, SObject {

  private final String name;
  private String icon;
  private final List<SClass> classes;

  /** It initiates the SPackage object with a name. */
  public SPackage(String name) {
    classes = new ArrayList<SClass>();
    this.name = name;
  }

  /** Adds a SClass object to the classes list of the SPackage. */
  public void addClass(SClass cls) {
    classes.add(cls);
  }

  /** It return the list of classes (SClass). */
  public List<SClass> classes() {
    return classes;
  }

  /** toString method returns the name of the package. */
  public String toString() {
    return name;
  }

  /** It configures an icon depending if the package is empty or not. **/
  public String icon() {
    if (this.classes.isEmpty()) {
      this.icon = "./resources/pack_empty_co.gif";
    } else {
      this.icon = "./resources/package_mode.gif";
    }
    return this.icon;
  }

  @Override
  public Font font() {
    return new Font("Helvetica", Font.PLAIN, 12);
  }

  @Override
  public Color background() {
    return Color.WHITE;
  }

  /** It provides a way to compare to packages by name. */
  @Override
  public int compareTo(Object obj) {
    if (obj instanceof SPackage) {
      return this.toString().compareTo(obj.toString());
    }
    return -3;
  }

  @Override
  public void accept(Visitor visitor) {
    visitor.visitPackage(this);
  }
}
