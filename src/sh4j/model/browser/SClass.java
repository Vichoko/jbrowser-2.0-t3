package sh4j.model.browser;

import org.eclipse.jdt.core.dom.TypeDeclaration;

import sh4j.model.visitor.Visitor;

import java.awt.Color;
import java.awt.Font;
import java.util.List;

/**
 * Class that wraps the information of a Class. It provides a series of methods
 * to extract information about an specific SClass object. Some methods are
 * methods(),isInterface(),superClass() plus SObject interface methods. It also
 * has a method compareTo who compares to SClass objects by name.
 **/
@SuppressWarnings("rawtypes")
public class SClass implements SObject, Comparable {
  private final TypeDeclaration declaration;
  private final List<SMethod> methods;
  /** A class indetation represent the hierarchy relation between the other classes in
   * the package.*/
  private int indentation;

  /** Initiates the obect from a declaration and a list of methods. 
   * The indentation level starts at 0, and the information is updated
   * when the classes are sorted by hierarchy.*/
  public SClass(TypeDeclaration td, List<SMethod> ms) {
    declaration = td;
    methods = ms;
    indentation = 0;
  }

  /** Return the list of methods the class contains. */
  public List<SMethod> methods() {
    return methods;
  }

  /** Return the class name. */
  public String className() {
    return declaration.getName().getIdentifier();
  }

  /**
   * It sets the indentantion level of the class. This indentantion level
   * represents the hierarchical relation between all the classes of the
   * package.
   * For example, a class C inherits from an abstract class B who inherits from
   * an abstract class C (C doesn't inherits). Then the indentation levels are
   * 2,1 and 0 resp.
   */
  public void setIndentation(int ind) {
    indentation = ind;
  }

  /** Returns true if the class is an Interface. */
  public boolean isInterface() {
    return declaration.isInterface();
  }

  /**
   * Returns the name of the superclass the class inherits from. If it doesnt
   * inherits directly from any class, then it inherits from Object.
   */
  public String superClass() {
    if (declaration.getSuperclassType() == null) {
      return "Object";
    }
    return declaration.getSuperclassType().toString();
  }

  /** toString method returns the name of the class. */
  public String toString() {
    String ind = "";
    for (int i = 0; i < indentation; i++) {
      ind = ind + "  ";
    }
    return ind + className();
  }

  /*
   * These two methods change the icon and the font depending if it is a class
   * or a interface.
   */
  @Override
  public String icon() {
    if (isInterface()) {
      return "./resources/int_obj.gif";
    } else {
      return "./resources/class_obj.gif";
    }
  }

  @Override
  public Font font() {
    if (isInterface()) {
      return new Font("Helvetica", Font.ITALIC, 12);
    } else {
      return new Font("Helvetica", Font.PLAIN, 12);
    }
  }

  @Override
  public Color background() {
    return Color.WHITE;
  }

  @Override
  public int compareTo(Object obj) {
    if (obj instanceof SClass) {
      return this.toString().compareTo(obj.toString());
    }
    return -3;
  }

  @Override
  public void accept(Visitor visitor) {
    visitor.visitClass(this);
  }

}
