package sh4j.model.browser;

import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;

import sh4j.model.format.SHTMLFormatter;
import sh4j.model.visitor.Visitor;
import sh4j.parser.model.SBlock;

import java.awt.Color;
import java.awt.Font;

/**
 * Class that wraps the information of a Method. It provides a series of methods
 * to extract information about an specific SMethod object. Some method are
 * name(),body(),getLinesOfCode() plus SObject interface methods.
 **/
public class SMethod implements SObject {
  private final MethodDeclaration declaration;
  private final SBlock body;
  private final SHTMLFormatter format;

  /**
   * Constructor of the class. It recieves a MethodDeclaration containing de
   * Declaration of the method, and a SBlock containing the body of the method.
   **/
  public SMethod(MethodDeclaration node, SBlock body) {
    declaration = node;
    this.body = body;
    this.body.export(format = new SHTMLFormatter());
  }

  /** It return a String with de modifier of the method. */
  public String modifier() {
    for (Object obj : declaration.modifiers()) {
      if (obj instanceof Modifier) {
        Modifier modifier = (Modifier) obj;
        return modifier.getKeyword().toString();
      }
    }
    return "default";
  }

  /** It returns the name of the method. */
  public String name() {
    return declaration.getName().getIdentifier();
  }

  /** It returns the body of the method. */
  public SBlock body() {
    return body;
  }

  /** It returns the name of the method. */
  public String toString() {
    return name();
  }

  /** It returns the number of lines of code of the method. */
  public int getLinesOfCode() {
    return format.getLines();
  }

  /** Depending on the modifier of the method, it returns an icon. */
  @Override
  public String icon() {
    String mod = modifier();
    if ("private".equals(mod)) {
      return "./resources/private_co.gif";
    } else if ("default".equals(mod)) {
      return "./resources/default_co.gif";
    } else if ("protected".equals(mod)) {
      return "./resources/protected_co.gif";
    } else {
      return "./resources/public_co.gif";
    }
  }

  @Override
  public Font font() {
    return new Font("Helvetica", Font.PLAIN, 12);
  }

  /**
   * The background colour relies on the quantity of lines that the method have.
   * +30 lines: Yellow. +50 lines: Red
   */
  @Override
  public Color background() {
    int lines = format.getLines();
    if (lines > 50) {
      return Color.RED;
    } else if (lines > 30) {
      return Color.YELLOW;
    } else {
      return Color.WHITE;
    }
  }

  @Override
  public void accept(Visitor visitor) {
    visitor.visitMethod(this);
  }

}
