package sh4j.model.browser;

import sh4j.model.visitor.Visitor;

import java.awt.Color;
import java.awt.Font;


/**
 * SObject provides an interface for elements of a java project that can be
 * showed in the visual interface.
 **/
public interface SObject {
  /**
   * It returns the respective icon PATH (in a String) depending of the SObject.
   */
  String icon();

  /** It returns the respective Font depending of the SObject and the case.. */
  Font font();

  /** It returns the respective Color depending of the SObject and the case. */
  Color background();

  /**
   * All SObject classes should have an accept method to correcly visit them
   * with any Visitor class.
   */
  void accept(Visitor visitor);
}
