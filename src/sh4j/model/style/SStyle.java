package sh4j.model.style;

/** Class that provides an interface for different styles and it highlights. */
public interface SStyle {

  public String toString();

  /**
   * It returns the HTML code of the text with Classname Highlight corresponding
   * of the Style.
   */
  public String formatClassName(String text);

  /**
   * It returns the HTML code of the text with Curly Brackets Highlight
   * corresponding of the Style.
   */
  public String formatCurlyBracket(String text);

  /**
   * It returns the HTML code of the text with KeyWord Highlight corresponding
   * of the Style.
   */
  public String formatKeyWord(String text);

  /**
   * It returns the HTML code of the text with PseudoVariable Highlight
   * corresponding of the Style.
   */
  public String formatPseudoVariable(String text);

  /**
   * It returns the HTML code of the text with Semicolon Highlight corresponding
   * of the Style.
   */
  public String formatSemiColon(String text);

  /**
   * It returns the HTML code of the text with String Highlight corresponding of
   * the Style.
   */
  public String formatString(String text);

  /**
   * It returns the HTML code of the text with MainCLass Highlight corresponding
   * of the Style.
   */
  public String formatMainClass(String text);

  /**
   * It returns the HTML code of the text with Modifier Highlight corresponding
   * of the Style.
   */
  public String formatModifier(String text);

  /**
   * It returns the HTML code of the text with correct Body settings depending
   * of the style.
   */
  public String formatBody(String text);

}
