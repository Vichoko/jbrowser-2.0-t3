package sh4j.model.highlight;

import sh4j.model.style.SStyle;

/**
 * Implements SHighlighter for any text that doesn't fill in any of the other
 * implemented filters. It returns the String in plain text.
 */
public class SDummy implements SHighlighter {

  @Override
  public boolean needsHighLight(String text) {
    return true;
  }

  @Override
  public String highlight(String text, SStyle style) {
    return text;
  }

}
