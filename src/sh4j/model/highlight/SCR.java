package sh4j.model.highlight;

import static sh4j.model.format.SHTMLFormatter.tag;

import sh4j.model.style.SStyle;

/**
 * The next methods format the Carriage Return line display depending of the
 * Style. the method is static because of the usage in the HTMLFormmater doesn't
 * need an explicit instantation of this class.
 **/
public abstract class SCR {
  /**
   * This method does nothing. Since every other method is static, its needed to
   * have at least one abstract method.
   */
  protected abstract void none();

  /** It highlights the number of line showed in the visual interface. */
  public static String highlight(String text, SStyle style) {
    return tag("span", text, "background:#f1f0f0;");
  }

}
