package sh4j.model.highlight;

import sh4j.model.style.SStyle;

/**
 * It provides the needsHighLight and highlight methods for different types of
 * coding words and simbols.
 */
public interface SHighlighter {
  /**
   * It returns true if the (String) text fills in the pattern of the Highlight.
   */

  public boolean needsHighLight(String text);

  /**
   * It returns a String with the Highlighted text, depending of the Style in
   * HTML.
   */

  public String highlight(String text, SStyle style);
}
