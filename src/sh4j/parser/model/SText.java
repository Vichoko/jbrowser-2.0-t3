package sh4j.parser.model;

import sh4j.model.format.SFormatter;
import sh4j.model.format.SHTMLFormatter;
import sh4j.model.format.SPlainFormatter;
import sh4j.model.highlight.SHighlighter;
import sh4j.model.style.SStyle;

public abstract class SText {

  public abstract void export(SFormatter format);

  public String toString() {
    SFormatter format = new SPlainFormatter();
    this.export(format);
    return format.formattedText();
  }

  /**
   * It recieves a style and highlights and formats the code to html. It also
   * recieves a boolean linenumbers, this var is passed to the formatter to
   * display (or not) the line numbers.
   */
  public String toHTML(SStyle style, boolean linenumbers, SHighlighter... lighters) {
    SFormatter format = new SHTMLFormatter(style, linenumbers, lighters);
    this.export(format);
    return format.formattedText();
  }
}