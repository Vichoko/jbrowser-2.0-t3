package sh4j.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SFactory;
import sh4j.model.browser.SMethod;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;
import sh4j.model.command.SCommand;
import sh4j.model.command.SCommandStyle;
import sh4j.model.highlight.SHighlighter;
import sh4j.model.style.SStyle;

/**
 * JBrowser
 * 
 * @author juampi
 *
 */
@SuppressWarnings({ "unchecked", "serial" })
public class SFrame extends JFrame {

  private SHighlighter[] lighters;
  protected SStyle style;
  private JList<SPackage> packages;
  private JList<SClass> classes;
  private JList<SMethod> methods;
  private List<SCommand> status;
  private JEditorPane htmlPanel;
  private JPanel footerbar;
  private JMenu file;
  private JMenu stylemenu;
  private JMenu togglemenu;
  private SProject project;
  private boolean linenumbers = true;

  /**
   * Create a browser, it will use the style and the lighters passed in the
   * argutments.
   * 
   * @param style
   * @param lighters
   */
  public SFrame(SStyle style, SHighlighter... lighters) {
    super("CC3002 Browser");
    status = new ArrayList<SCommand>();
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setResizable(false);
    setSize(450, 750);
    build();
    this.lighters = lighters;
    this.style = style;
    JMenuBar bar = new JMenuBar();
    file = new JMenu("Sort");
    /** Agregado para manejar estilos. */
    stylemenu = new JMenu("Styles");
    /** Agregando para manejar toggles. */
    togglemenu = new JMenu("Toggle");
    bar.add(file);
    bar.add(stylemenu);
    bar.add(togglemenu);
    addToggleLineNumbers();
    setJMenuBar(bar);

    /**
     * Status almacena los contadores de la barra de estado (footer), para
     * actualizarlos una vez se cargue el proyecto.
     */

  }

  /**
   * It adds an array of SCommands applied to the project to be shown in the
   * footer status bar
   */
  public void addFooters(SCommand... fts) {
    for (SCommand f : fts) {
      addFooter(f);
    }
  }

  /** The individual version of the addFooters method. */
  private void addFooter(final SCommand f) {
    status.add(f);
  }

  /**
   * This method responsability is to update the status bar elements when the
   * project is correctly set up.
   */
  private void updateFooter() {
    for (SCommand c : status) {
      c.executeOn(project);
      JLabel item = new JLabel(c.toString());
      footerbar.add(item);
    }
    getContentPane().revalidate();
  }

  /**
   * Add commands to the menu bar
   * 
   * @param cmds
   */
  public void addCommands(SCommand... cmds) {
    for (SCommand c : cmds) {
      addCommand(c);
    }
  }

  /** The individual version of the past method. */
  private void addCommand(final SCommand c) {
    JMenuItem item = new JMenuItem(c.name());
    item.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (project != null) {
          c.executeOn(project);
          update(project);
        }
      }
    });
    file.add(item);
  }

  /**
   * Add styles to the menu bar
   * 
   * @param cmds
   */
  public void addStyles(SCommandStyle... cmds) {
    for (SCommandStyle c : cmds) {
      addStyle(c);
    }
  }

  private void addStyle(final SCommandStyle c) {
    JMenuItem item = new JMenuItem(c.name());
    item.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        lighters = c.getHighlights();
        style = c.getStyle();
        style(style);
      }
    });
    stylemenu.add(item);
  }

  /**
   * Adds the Toggle line numbers button to the toggle menu. It changes the
   * value of the linenumbers variable. This variable is passed to the
   * formatter, then the formatter formats the text depending of the state of
   * the variable. true = line numbers on and false off.
   */
  private void addToggleLineNumbers() {
    JMenuItem item = new JMenuItem("Toggle Line Numbers");
    item.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (linenumbers)
          linenumbers = false;
        else
          linenumbers = true;
        style(style);
      }
    });
    togglemenu.add(item);
  }

  public void update(JList list, List data) {
    list.setListData(data.toArray());
    if (data.size() > 0) {
      list.setSelectedIndex(0);
    }
  }

  private void build() {
    buildPathPanel();
    methods = new JList<SMethod>();
    addOn(methods, BorderLayout.EAST);
    classes = new JList<SClass>();
    addOn(classes, BorderLayout.CENTER);
    packages = new JList<SPackage>();
    addOn(packages, BorderLayout.WEST);

    htmlPanel = buildHTMLPanel(BorderLayout.SOUTH);

    packages.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (packages.getSelectedIndex() != -1) {
          update(classes, packages.getSelectedValue().classes());
        }
      }
    });
    classes.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (classes.getSelectedIndex() != -1) {
          update(methods, classes.getSelectedValue().methods());
        }
      }
    });
    methods.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (methods.getSelectedIndex() != -1) {
          SMethod method = methods.getSelectedValue();
          htmlPanel.setText(method.body().toHTML(style, linenumbers, lighters));
        }
      }
    });

  }

  private void addOn(JList list, String direction) {
    list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    list.setLayoutOrientation(JList.VERTICAL);
    list.setCellRenderer(new SItemRenderer());
    JScrollPane pane = new JScrollPane(list);
    pane.setMinimumSize(new Dimension(200, 200));
    pane.setPreferredSize(new Dimension(200, 200));
    pane.setMaximumSize(new Dimension(200, 200));
    getContentPane().add(pane, direction);
  }

  private void buildPathPanel() {
    JPanel pathPanel = new JPanel();
    pathPanel.setLayout(new BorderLayout());
    final JTextField pathField = new JTextField();
    pathField.setEnabled(false);
    JButton browseButton = new JButton("Browse");
    browseButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle("Select a root folder");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showOpenDialog(SFrame.this) == JFileChooser.APPROVE_OPTION) {
          pathField.setText(chooser.getSelectedFile().getPath());
          try {
            project = new SFactory().create(chooser.getSelectedFile().getPath());
            update(packages, project.packages());
            if (project.packages().size() > 0) {
              packages.setSelectedIndex(0);
            }
            updateFooter();
          } catch (IOException ex) {

          }
        }
      }
    });
    pathPanel.add(pathField, BorderLayout.CENTER);
    pathPanel.add(browseButton, BorderLayout.EAST);
    getContentPane().add(pathPanel, BorderLayout.NORTH);
  }

  private JEditorPane buildHTMLPanel(String direction) {
    /* A new panel is created to manage the html panel and the footer bar. */
    JPanel southpanel = new JPanel(new BorderLayout());
    JEditorPane htmlPanel = new JEditorPane();
    htmlPanel.setEditable(false);
    htmlPanel.setMinimumSize(new Dimension(600, 300));
    htmlPanel.setPreferredSize(new Dimension(600, 300));
    htmlPanel.setMaximumSize(new Dimension(600, 300));
    htmlPanel.setContentType("text/html");
    /* The html pane is added to the panel. */
    southpanel.add(
        new JScrollPane(htmlPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS),
        BorderLayout.CENTER);
    /* The footer bar is added too. */
    southpanel.add(buildFooterBar(), BorderLayout.SOUTH);
    getContentPane().add(southpanel, direction);
    return htmlPanel;
  }

  /**
   * It build the footerbar from a JPanel and saves it reference in footerbar
   * instance var.
   */
  private Component buildFooterBar() {
    JPanel footerpanel = new JPanel();
    footerbar = footerpanel;
    return footerpanel;

  }

  public void update(SProject project) {
    SPackage pkg = packages.getSelectedValue();
    SClass cls = classes.getSelectedValue();
    SMethod method = methods.getSelectedValue();
    packages.setListData(project.packages().toArray(new SPackage[] {}));
    packages.setSelectedValue(pkg, true);
    if (pkg != null) {
      classes.setListData(pkg.classes().toArray(new SClass[] {}));
      classes.setSelectedValue(cls, true);
      if (cls != null) {
        methods.setListData(cls.methods().toArray(new SMethod[] {}));
        methods.setSelectedValue(method, true);
      }
    }
  }

  /** It re-formats the code depending of the style. */
  public void style(SStyle style) {
    this.style = style;
    SMethod method = methods.getSelectedValue();
    htmlPanel.setText(method.body().toHTML(style, linenumbers, lighters));
  }
}
